#include <stdio.h>
int main(){
	float h, r, PI, vlm; 
	printf("Enter Radius:\n");
	scanf("%f",&r);
	printf("Enter Height:\n");
	scanf("%f",&h);
	
	PI = 3.14;
	vlm = PI*r*r*h/3;
	
	printf("Volume of the Cone (in cubic units) = %.2f\n", vlm);
	
	return 0;
}