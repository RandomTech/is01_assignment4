#include <stdio.h>
int main(){
	unsigned int A=10 ,B=15, C; 
	printf("Value of A is %d \n",A);
	printf("Value of B is %d \n",B);
	
	C=A&B;
	printf("%d & %d = %d\n",A,B,C);
	
	C=A^B;
	printf("%d ^ %d = %d\n",A,B,C);
	
	C=~A;
	printf("~%d = %d\n",A,C);
	
	C=A<<3;
	printf("%d << 3 = %d\n",A,C);
	
	C=B>>3;
	printf("%d >> 3 = %d\n",B,C);
	
	return 0;
}